#include <unistd.h>
#include <stdio.h>

int main(){


	int var;
    //fork() This function cretes a new process. The return value is the zero in the child
    //and the process-id number of the child in the parent, or -1 upon error.
	if (fork() == 0){
		//child
		var = 5;
		printf("child:%d\n",var);
	}else{
		//parent
		printf("parent:%d\n",var);

	}
}