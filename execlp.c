#include <stdio.h>
#include <unistd.h>

int main(){
	
	int pd[2]; //pipe descriptor

    //pipe() create a pipe , a unidirectional data channel that can be used for interprocess communictation
	pipe(pd);

	if(fork() == 0){
        //close() closes a file descriptor, so that it no longer refers to any file and may be reused.
		close(1);
        //The dup() system call creates a copy of the file descriptor oldfb , susing the losest-numbered
        //unused descriptor for the new descriptor
 		dup(pd[1]); //the output is now sent to pipe
		close(pd[0]); // we are not going to read from pipe
		execlp("ls","ls","-l",NULL);
		//child
	}else{
		close(0);
		dup(pd[0]); //the input in now recvd from pipe
		close(pd[1]); //we are not going to write to the pipe
		//parent
		execlp("wc","wc","-l",NULL);
	}

	return 0;
}